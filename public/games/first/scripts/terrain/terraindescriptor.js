export class TerrainDescriptor {
    constructor(name, description, verbs, weight, instantiator) {
        this.name = name
        this.description = description
        this.verbs = verbs
        this.weight = weight
        this.instantiator = instantiator
    }
    getName() {
        return this.name
    }
    getDescription(){
        return this.description
    }
    hasVerb(verb) {
        return Object.keys(this.verbs).find(x => x == verb) != null
    }
    getVerbs() {
        return Object.keys(this.verbs)
    }
    getWeight() {
        return this.weight
    }
    instantiate(){
        return this.instantiator()
    }
    doVerb(avatar,area,verb){
        if(avatar!=null && area!=null && avatar.canDoVerb(verb) && this.hasVerb(verb)){
            this.verbs[verb](avatar,area)
        }
    }
}