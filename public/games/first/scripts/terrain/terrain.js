import { Utility } from "../utility.js"
import { TerrainDescriptor } from "./terraindescriptor.js"
import { TERRAIN_GRASS, TERRAIN_WOODS, TERRAIN_ROCKS } from "./terrainnames.js"
import { TerrainInstantiators } from "./terraininstantiators.js"
import { TerrainVerbifiers } from "./terrainverbifiers.js"

let descriptors = {};
descriptors[TERRAIN_WOODS] = new TerrainDescriptor(TERRAIN_WOODS, "a wooded area", TerrainVerbifiers.createTree(), 2, TerrainInstantiators.createTree)
descriptors[TERRAIN_GRASS] = new TerrainDescriptor(TERRAIN_GRASS, "a grassy area", TerrainVerbifiers.createGrass(), 10, TerrainInstantiators.createGrass)
descriptors[TERRAIN_ROCKS] = new TerrainDescriptor(TERRAIN_GRASS, "a rocky area", TerrainVerbifiers.createRocks(), 1, TerrainInstantiators.createRocks)

let generator = {};
for (let key in descriptors) {
    generator[key] = descriptors[key].getWeight()
}

export class Terrain {
    constructor(area, data) {
        this.area = area
        this.data = data
    }
    getArea() {
        return this.area
    }
    getName() {
        return this.data.name
    }
    static generate() {
        let terrainName = Utility.randomWeighted(generator)
        let descriptor = descriptors[terrainName]
        let result = descriptor.instantiate();
        return result
    }
    static getDescriptor(terrain) {
        return descriptors[terrain];
    }
}