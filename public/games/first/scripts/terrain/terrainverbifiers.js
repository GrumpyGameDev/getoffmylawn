import { VERB_FORAGE } from "../verb/verbs.js"
import { TerrainVerbActions } from "./terrainverbactions.js"
export class TerrainVerbifiers {
    static createTree() {
        let verbs = {}
        verbs[VERB_FORAGE] = TerrainVerbActions.forageTree
        return verbs
    }
    static createGrass() {
        let verbs = {}
        verbs[VERB_FORAGE] = TerrainVerbActions.forageGrass
        return verbs
    }
    static createRocks() {
        let verbs = {}
        verbs[VERB_FORAGE] = TerrainVerbActions.forageRocks
        return verbs
    }
}
