import { TERRAIN_GRASS, TERRAIN_WOODS, TERRAIN_ROCKS } from "./terrainnames.js"

export class TerrainInstantiators {
    static createTree() {
        return {
            name: TERRAIN_WOODS,
            displayName: "tree"
        }
    }
    static createRocks() {
        return {
            name: TERRAIN_ROCKS,
            displayName: "rocks"
        }
    }
    static createGrass() {
        return {
            name: TERRAIN_GRASS,
            displayName: "grass"
        }
    }
}