import { ITEM_PLANT_FIBER, ITEM_SMALL_BRANCH, ITEM_STONE } from "../item/itemnames.js"
import { SKILL_FORAGE_TREE, SKILL_FORAGE_GRASS, SKILL_FORAGE_ROCKS } from "../skill/skillnames.js"
export class TerrainVerbActions {
    static forageTree(avatar, area) {
        if(avatar.skillCheck(SKILL_FORAGE_TREE)){
            avatar.getInventory().add(ITEM_SMALL_BRANCH, 1)
            avatar.addMessage("You forage in the tree and find a stick.")
        }else{
            avatar.addMessage("You forage in the tree and find nothing.")
        }
        avatar.addTurn()
    }

    static forageGrass(avatar, area) {
        if(avatar.skillCheck(SKILL_FORAGE_GRASS)){
            avatar.getInventory().add(ITEM_PLANT_FIBER, 1)
            avatar.addMessage("You forage in the grass and find some plant fiber.")
        }else{
            avatar.addMessage("You forage in the grass and find nothing.")
        }
        avatar.addTurn()
    }

    static forageRocks(avatar, area) {
        if(avatar.skillCheck(SKILL_FORAGE_ROCKS)){
            avatar.getInventory().add(ITEM_STONE, 1)
            avatar.addMessage("You forage in the rocks and find a stone.")
        }else{
            avatar.addMessage("You forage in the rocks and find nothing.")
        }
        avatar.addTurn()
    }
}

