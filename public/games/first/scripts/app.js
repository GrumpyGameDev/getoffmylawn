import { World } from "./world/world.js"
import { View } from "./view/view.js"
export class App {
    static main() {
        World.create();
        View.showStatus();
    }
}
document.addEventListener("DOMContentLoaded", () => {
    App.main();
});