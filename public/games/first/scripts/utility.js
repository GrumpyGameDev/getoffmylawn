export class Utility {
    static randomMaximum(maximum) {
        return Math.floor(Utility.randomFloat(maximum))
    }
    static randomFloat(maximum) {
        return Math.random() * maximum
    }
    static randomWeighted(table) {
        let total = 0
        for (let key in table) {
            total += table[key]
        }
        let generated = Utility.randomMaximum(total)
        for (let key in table) {
            if (generated < table[key]) {
                return key;
            } else {
                generated -= table[key]
            }
        }
    }
    static toPercentage(value) {
        return value * 100
    }
}