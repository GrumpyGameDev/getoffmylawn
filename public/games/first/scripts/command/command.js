import { World } from "../world/world.js"
import { Direction } from "../world/direction.js"
import { View } from "../view/view.js"

export const COMMAND_TURN = "turn"
export const COMMAND_MOVE = "move"
export const COMMAND_REST = "rest"
export const COMMAND_VERB = "verb"
export const COMMAND_SET_VIEW = "set-view"
export const COMMAND_DROP = "drop"
export const COMMAND_TAKE = "take"

export const FACING_LEFT = "left"
export const FACING_RIGHT = "right"
export const FACING_AROUND = "around"

export const MOVE_BACK = "back"

export class Command {

    static doTurn(tokens) {
        let avatar = World.get().getAvatar()
        let facing = tokens.shift()
        switch (facing) {
            case FACING_AROUND:
                avatar.setFacing(Direction.turnAround(avatar.getFacing()))
                avatar.addMessage("You turn around.")
                View.showStatus()
                break
            case FACING_LEFT:
                avatar.setFacing(Direction.turnLeft(avatar.getFacing()))
                avatar.addMessage("You turn left.")
                View.showStatus()
                break
            case FACING_RIGHT:
                avatar.setFacing(Direction.turnRight(avatar.getFacing()))
                avatar.addMessage("You turn right.")
                View.showStatus()
                break
        }
    }

    static doRest(tokens) {
        let avatar = World.get().getAvatar()
        avatar.addTurn()
        avatar.addMessage("You rest for a spell.")
        View.showStatus()
    }

    static doVerb(tokens) {
        let verb = tokens.shift()
        let avatar = World.get().getAvatar()
        avatar.doVerb(verb)
        View.showStatus()
    }

    static doMove(tokens) {
        let avatar = World.get().getAvatar()
        if (tokens.length == 0) {
            avatar.setPosition(Direction.step(avatar.getFacing(), avatar.getPosition()))
            avatar.addMessage("You move ahead.")
            avatar.addTurn()
            View.showStatus()
        } else {
            switch (tokens[0]) {
                case FACING_LEFT:
                    avatar.setPosition(Direction.step(Direction.turnLeft(avatar.getFacing()), avatar.getPosition()))
                    avatar.addMessage("You move left.")
                    avatar.addTurn()
                    View.showStatus()
                    break
                case FACING_RIGHT:
                    avatar.setPosition(Direction.step(Direction.turnRight(avatar.getFacing()), avatar.getPosition()))
                    avatar.addMessage("You move right.")
                    avatar.addTurn()
                    View.showStatus()
                    break
                case MOVE_BACK:
                    avatar.setPosition(Direction.step(Direction.turnAround(avatar.getFacing()), avatar.getPosition()))
                    avatar.addMessage("You move back.")
                    avatar.addTurn()
                    View.showStatus()
                    break
            }
        }
    }

    static setView(tokens) {
        World.get().getAvatar().setViewState(tokens)
        View.showStatus()
    }

    static doTake(tokens) {
        let item = tokens.shift()
        let count = Number(tokens.shift())
        let avatar = World.get().getAvatar()
        let area = World.get().getAvatarArea()
        avatar.getInventory().add(item, count)
        area.getInventory().remove(item, count)
        View.showStatus()
    }

    static doDrop(tokens) {
        let item = tokens.shift()
        let count = Number(tokens.shift())
        let avatar = World.get().getAvatar()
        let area = World.get().getAvatarArea()
        avatar.getInventory().remove(item, count)
        area.getInventory().add(item, count)
        View.showStatus()
    }

    static doCommand(tokens) {
        let command = tokens.shift()
        switch (command) {
            case COMMAND_TURN:
                Command.doTurn(tokens)
                break
            case COMMAND_MOVE:
                Command.doMove(tokens)
                break
            case COMMAND_REST:
                Command.doRest(tokens)
                break
            case COMMAND_VERB:
                Command.doVerb(tokens)
                break
            case COMMAND_SET_VIEW:
                Command.setView(tokens)
                break
            case COMMAND_TAKE:
                Command.doTake(tokens)
                break
            case COMMAND_DROP:
                Command.doDrop(tokens)
                break
        }
    }
}
window.doCommand = Command.doCommand