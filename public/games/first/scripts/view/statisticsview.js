import { VIEW_NAVIGATION, STATISTICSVIEW_SKILLS } from "./viewstates.js"
import { Display } from "./display.js   "
import { COMMAND_SET_VIEW } from "../command/command.js"
import { Skill } from "../skill/skill.js"
import { Utility } from "../utility.js"

export class StatisticsView {
    static generateSkills(avatar, viewState) {
        let content = "<h2>Skills</h2>"
        let skills = Skill.getList()
        for (let skill of skills) {
            let descriptor = Skill.getDescriptor(skill)
            content += `<p>${descriptor.getDisplayName()} ${Utility.toPercentage(avatar.getSkill(skill).getValue()).toFixed(2)}%</p>`
        }
        return content
    }
    static generate(avatar, viewState) {
        let statisticsView = viewState.shift()
        let content = ""
        switch (statisticsView) {
            case STATISTICSVIEW_SKILLS:
                content += StatisticsView.generateSkills(avatar, viewState)
                break
        }
        content += `<p>${Display.commandButton("Go Back", [COMMAND_SET_VIEW, VIEW_NAVIGATION])}</p>`
        return content
    }
}