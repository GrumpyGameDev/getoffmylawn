export const VIEW_NAVIGATION = "navigation"
export const VIEW_INVENTORY = "inventory"
export const VIEW_STATISTICS = "statistics"

export const INVENTORYVIEW_AVATAR = "avatar"
export const INVENTORYVIEW_AREA = "area"

export const STATISTICSVIEW_SKILLS = "skills"