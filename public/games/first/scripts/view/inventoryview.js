import { INVENTORYVIEW_AREA, INVENTORYVIEW_AVATAR, VIEW_NAVIGATION } from "./viewstates.js"
import { Display } from "./display.js   "
import { COMMAND_SET_VIEW, COMMAND_DROP, COMMAND_TAKE } from "../command/command.js"
import { Items } from "../item/items.js"
import { World } from "../world/world.js"
export class InventoryView {
    static generateAvatarInventory(avatar, viewState) {
        let content = ""
        content += "<h3>Inventory</h3>"
        let inventory = avatar.getInventory()
        let items = inventory.getList()
        for (let item of items) {
            let descriptor = Items.getDescriptor(item)
            let count = inventory.getCount(item)
            content += `<p>${descriptor.getDisplayName()} ${count} `
            let amount = 1
            while (amount < count) {
                content += Display.commandButton(`Drop ${amount}`, [COMMAND_DROP, item, `${amount}`])
                content += " "
                amount *= 10
            }
            content += Display.commandButton(`Drop All`, [COMMAND_DROP, item, `${count}`])
            content += `</p>`
        }
        return content
    }
    static generateAreaInventory(avatar, viewState) {
        let content = ""
        content += "<h3>On Ground</h3>"
        let inventory = World.get().getAvatarArea().getInventory()
        let items = inventory.getList()
        for (let item of items) {
            let descriptor = Items.getDescriptor(item)
            let count = inventory.getCount(item)
            content += `<p>${descriptor.getDisplayName()} ${count} `
            let amount = 1
            while (amount < count) {
                content += Display.commandButton(`Take ${amount}`, [COMMAND_TAKE, item, `${amount}`])
                content += " "
                amount *= 10
            }
            content += Display.commandButton(`Take All`, [COMMAND_TAKE, item, `${count}`])
            content += `</p>`
        }
        return content
    }
    static generate(avatar, viewState) {
        let inventoryView = viewState.shift()
        let content = ""
        switch (inventoryView) {
            case INVENTORYVIEW_AVATAR:
                content += InventoryView.generateAvatarInventory(avatar, viewState)
                break
            case INVENTORYVIEW_AREA:
                content += InventoryView.generateAreaInventory(avatar, viewState)
                break
        }
        content += `<p>${Display.commandButton("Go Back", [COMMAND_SET_VIEW, VIEW_NAVIGATION])}</p>`
        return content
    }
}