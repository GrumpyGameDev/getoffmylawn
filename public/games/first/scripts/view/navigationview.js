import { World } from "../world/world.js"
import { COMMAND_MOVE, FACING_LEFT, FACING_AROUND, FACING_RIGHT, COMMAND_TURN, MOVE_BACK, COMMAND_REST, COMMAND_VERB, COMMAND_SET_VIEW } from "../command/command.js"
import { Terrain } from "../terrain/terrain.js"
import { Verbs } from "../verb/verbs.js"
import { Display } from "./display.js"
import { VIEW_INVENTORY, INVENTORYVIEW_AVATAR, VIEW_STATISTICS, STATISTICSVIEW_SKILLS, INVENTORYVIEW_AREA } from "./viewstates.js"
export class NavigationView {
    static generate(avatar, viewState) {
        let area = World.get().getAvatarArea()
        let descriptor = Terrain.getDescriptor(area.getTerrain().getName())
        avatar.clearMessages()
        let content = `<p>You are in ${descriptor.getDescription()}.</p>`
        content += `<p>Move: `
        content += Display.commandButton("&larr;", [COMMAND_MOVE, FACING_LEFT])
        content += Display.commandButton("&uarr;", [COMMAND_MOVE])
        content += Display.commandButton("&darr;", [COMMAND_MOVE, MOVE_BACK])
        content += Display.commandButton("&rarr;", [COMMAND_MOVE, FACING_RIGHT])
        content += `</p>`
        content += `<p>Turn: `
        content += Display.commandButton("Turn &larr;", [COMMAND_TURN, FACING_LEFT])
        content += Display.commandButton("Turn &darr;", [COMMAND_TURN, FACING_AROUND])
        content += Display.commandButton("Turn &rarr;", [COMMAND_TURN, FACING_RIGHT])
        content += `</p>`
        content += `<p>Actions: `
        content += Display.commandButton("Rest", [COMMAND_REST])
        let verbs = descriptor.getVerbs()
        for (let verb of verbs) {
            if (avatar.canDoVerb(verb)) {
                let descriptor = Verbs.getDescriptor(verb)
                content += Display.commandButton(descriptor.getDisplayName(), [COMMAND_VERB, descriptor.getName()])
            }
        }
        content += `</p>`
        content += `<p>Statuses: `
        content += Display.commandButton("Skills", [COMMAND_SET_VIEW, VIEW_STATISTICS, STATISTICSVIEW_SKILLS])
        if (!avatar.getInventory().isEmpty()) {
            content += Display.commandButton("Inventory", [COMMAND_SET_VIEW, VIEW_INVENTORY, INVENTORYVIEW_AVATAR])
        }
        if (!area.getInventory().isEmpty()) {
            content += Display.commandButton("On Ground", [COMMAND_SET_VIEW, VIEW_INVENTORY, INVENTORYVIEW_AREA])
        }
        content += `</p>`
        return content
    }
}
