export class Display {
    static setContent(content) {
        document.body.innerHTML = content;
    }
    static commandButton(title, tokens) {
        return `<button onclick="doCommand(['${tokens.join("','")}'])">${title}</button>`
    }
}
