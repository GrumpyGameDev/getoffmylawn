import { World } from "../world/world.js"
import { Display } from "./display.js"
import { VIEW_NAVIGATION, VIEW_INVENTORY, VIEW_STATISTICS } from "./viewstates.js"
import { NavigationView } from "./navigationview.js"
import { InventoryView } from "./inventoryview.js"
import { StatisticsView } from "./statisticsview.js"
export class View {
    static showStatus() {
        let avatar = World.get().getAvatar();
        let content = "";
        let messages = avatar.getMessages()
        for (let message of messages) {
            content += `<p><i>${message}</i></p>`
        }
        let viewState = avatar.getViewState()
        let mainView = viewState.shift()
        switch (mainView) {
            case VIEW_NAVIGATION:
                content += NavigationView.generate(avatar, viewState)
                break
            case VIEW_INVENTORY:
                content += InventoryView.generate(avatar, viewState)
                break
            case VIEW_STATISTICS:
                content += StatisticsView.generate(avatar, viewState)
                break
        }
        Display.setContent(content)
    }
}
