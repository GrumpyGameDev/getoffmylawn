export class VerbDescriptor {
    constructor(name, displayName, skill) {
        this.name = name
        this.displayName = displayName
        this.skill = skill
    }
    getName(){
        return this.name
    }
    getDisplayName(){
        return this.displayName
    }
    getSkill(){
        return this.skill
    }
}