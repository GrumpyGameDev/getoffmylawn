import { VerbDescriptor } from "./verbdescriptor.js"
export const VERB_FORAGE = "forage"
let descriptors = {}
descriptors[VERB_FORAGE] = new VerbDescriptor(VERB_FORAGE,"Forage")
export class Verbs{
    static getDescriptor(verb){
        return descriptors[verb]
    }
}