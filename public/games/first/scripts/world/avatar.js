import { VIEW_NAVIGATION } from "../view/viewstates.js"
import { VERB_FORAGE } from "../verb/verbs.js"
import { DIRECTION_COUNT } from "./direction.js"
import { Utility } from "../utility.js"
import { Terrain } from "../terrain/terrain.js"
import { Inventory } from "../item/inventory.js"
import { Skill } from "../skill/skill.js"
export class Avatar {
    constructor(world, data) {
        this.data = data
        this.world = world
    }
    getWorld() {
        return this.world
    }
    getPosition() {
        return {
            x: this.data.x,
            y: this.data.y
        };
    }
    setPosition({ x, y }) {
        this.data.x = x
        this.data.y = y
    }
    getFacing() {
        return this.data.facing
    }
    setFacing(facing) {
        this.data.facing = facing
    }
    getTurn() {
        return this.data.turn
    }
    addTurn() {
        this.data.turn++
    }
    canDoVerb(verb) {
        return verb == VERB_FORAGE
    }
    clearMessages() {
        this.data.messages = []
    }
    addMessage(message) {
        this.data.messages.push(message)
    }
    getMessages() {
        return this.data.messages
    }
    getInventory() {
        return new Inventory(this, this.data.inventory)
    }
    doVerb(verb) {
        if (this.canDoVerb(verb)) {
            let area = this.getWorld().getArea(this.getPosition())
            let descriptor = Terrain.getDescriptor(area.getTerrain().getName())
            if (descriptor.hasVerb(verb)) {
                descriptor.doVerb(this, area, verb)
            } else {
                this.addMessage("You cannot do that here.")
            }
        } else {
            this.addMessage("You cannot do that.")
        }
    }
    getViewState() {
        return [...this.data.viewState]
    }
    setViewState(viewState) {
        this.data.viewState = [...viewState]
    }
    getSkill(skill) {
        let result = this.data.skills[skill]
        if (result == null) {
            let descriptor = Skill.getDescriptor(skill)
            if (descriptor) {
                result = descriptor.instantiate()
                this.data.skills[skill] = result
            }
        }
        return new Skill(this, result)
    }
    skillCheck(skill) {
        let result = Math.random() < this.getSkill(skill).getValue()
        if (!result) {
            this.getSkill(skill).addExperience(1)
        }
        return result
    }
    static create() {
        return {
            turn: 0,
            x: 0,
            y: 0,
            facing: Utility.randomMaximum(DIRECTION_COUNT),
            messages: [],
            inventory: Inventory.create(),
            viewState: [VIEW_NAVIGATION],
            skills: {}
        }
    }
}