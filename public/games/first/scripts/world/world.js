import { Avatar } from "./avatar.js"
import { Area } from "./area.js"

let worldData;
export class World {
    constructor(data) {
        this.data = data;
    }

    getAvatar() {
        return new Avatar(this, this.data.avatar);
    }

    getArea({ x, y }) {
        x = `${x}`;
        y = `${y}`;
        if (this.data.areas[x] == null) {
            this.data.areas[x] = {};
        }
        if (this.data.areas[x][y] == null) {
            this.data.areas[x][y] = Area.create();
        }
        return new Area(this, this.data.areas[x][y]);
    }

    getAvatarArea() {
        return this.getArea(this.getAvatar().getPosition())
    }

    static create() {
        worldData = {
            avatar: Avatar.create(),
            areas: {}
        };
    }

    static get() {
        return new World(worldData);
    }
}
