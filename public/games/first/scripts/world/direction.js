export const DIRECTION_NORTH = 0;
export const DIRECTION_EAST = 1;
export const DIRECTION_SOUTH = 2;
export const DIRECTION_WEST = 3;
export const DIRECTION_COUNT = 4;
export class Direction {
    static turnLeft(direction) {
        return (direction + DIRECTION_COUNT - 1) % DIRECTION_COUNT;
    }
    static turnRight(direction) {
        return (direction + 1) % DIRECTION_COUNT;

    }
    static turnAround(direction) {
        return (direction + DIRECTION_COUNT / 2) % DIRECTION_COUNT;
    }
    static step(direction, { x, y }) {
        switch (direction) {
            case DIRECTION_NORTH:
                return { x: x, y: y - 1 };
            case DIRECTION_EAST:
                return { x: x + 1, y: y };
            case DIRECTION_SOUTH:
                return { x: x, y: y + 1 };
            case DIRECTION_WEST:
                return { x: x - 1, y: y };
        }
    }
}
