import { Terrain } from "../terrain/terrain.js"
import { Inventory } from "../item/inventory.js"
export class Area {
    constructor(world, data) {
        this.world = world
        this.data = data
    }

    getWorld() {
        return this.world
    }

    getTerrain() {
        return new Terrain(this, this.data.terrain)
    }

    getInventory() {
        return new Inventory(this, this.data.inventory)
    }

    doVerb(verb) {

    }

    static create() {
        let terrain = Terrain.generate()
        return {
            terrain: terrain,
            inventory: Inventory.create()
        };
    }
}