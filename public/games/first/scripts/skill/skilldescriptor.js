export class SkillDescriptor {
    constructor(name, displayName, initialValue, experiencePerLevel) {
        this.name = name
        this.displayName = displayName
        this.initialValue = initialValue
        this.experiencePerLevel = experiencePerLevel
    }
    getName() {
        return this.name
    }
    getDisplayName() {
        return this.displayName
    }
    calculateValue(experience) {
        let level = experience / this.experiencePerLevel
        return Math.pow(this.initialValue, 1 / (1+level))
    }
    instantiate() {
        return {
            name: this.name,
            experience: 0
        }
    }
}