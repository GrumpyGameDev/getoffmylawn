import { SKILL_FORAGE_GRASS, SKILL_FORAGE_ROCKS, SKILL_FORAGE_TREE } from "./skillnames.js"
import { SkillDescriptor } from "./skilldescriptor.js"

let descriptors = {}
descriptors[SKILL_FORAGE_GRASS] = new SkillDescriptor(SKILL_FORAGE_GRASS, "Forage (Grass)", 0.5, 10)
descriptors[SKILL_FORAGE_ROCKS] = new SkillDescriptor(SKILL_FORAGE_ROCKS, "Forage (Rocks)", 0.5, 10)
descriptors[SKILL_FORAGE_TREE] = new SkillDescriptor(SKILL_FORAGE_TREE, "Forage (Tree)", 0.5, 10)
export class Skill {
    constructor(parent, data) {
        this.parent = parent
        this.data = data
    }
    getName() {
        return this.data.name
    }
    getParent() {
        return this.parent
    }
    getExperience() {
        return this.data.experience
    }
    addExperience(amount) {
        this.data.experience += amount
    }
    getValue() {
        let experience = this.getExperience()
        let descriptor = Skill.getDescriptor(this.getName())
        return descriptor.calculateValue(experience)

    }
    static getDescriptor(skill) {
        return descriptors[skill]
    }
    static getList() {
        return Object.keys(descriptors)
    }
}