export const SKILL_CRAFT_TWINE = "craft-twine"
export const SKILL_CRAFT_STICK = "craft-stick"
export const SKILL_CRAFT_SHARP_STONE = "craft-sharp-stone"
export const SKILL_CRAFT_STONE_AXE = "craft-stone-axe"

export const SKILL_FORAGE_GRASS = "forage-grass"
export const SKILL_FORAGE_ROCKS = "forage-rocks"
export const SKILL_FORAGE_TREE = "forage-tree"
