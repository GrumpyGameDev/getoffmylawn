export class ItemDescriptor {
    constructor(name, displayName) {
        this.name = name
        this.displayName = displayName
    }
    getName() {
        return this.name
    }
    getDisplayName(){
        return this.displayName
    }
}