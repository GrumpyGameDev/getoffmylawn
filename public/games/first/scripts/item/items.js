import { ITEM_PLANT_FIBER, ITEM_SMALL_BRANCH, ITEM_STONE } from "./itemnames.js"
import { ItemDescriptor } from "./itemdescriptor.js"

let descriptors = {}
descriptors[ITEM_PLANT_FIBER] = new ItemDescriptor(ITEM_PLANT_FIBER, "Plant Fiber")
descriptors[ITEM_SMALL_BRANCH] = new ItemDescriptor(ITEM_SMALL_BRANCH, "Small Branch")
descriptors[ITEM_STONE] = new ItemDescriptor(ITEM_STONE, "Stone")

export class Items {
    static getDescriptor(item) {
        return descriptors[item]
    }
}