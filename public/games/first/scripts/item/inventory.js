export class Inventory {
    constructor(parent, data) {
        this.parent = parent
        this.data = data
    }
    getParent() {
        return this.parent
    }
    isEmpty() {
        return Object.keys(this.data).length == 0
    }
    getList() {
        return [...Object.keys(this.data)]
    }
    getCount(item) {
        let result = this.data[item] || 0
        return result
    }
    setCount(item, count) {
        if (count > 0) {
            this.data[item] = count
        } else {
            delete this.data[item]
        }
    }
    add(item, count) {
        this.setCount(item, this.getCount(item) + count)
    }
    remove(item, count) {
        this.setCount(item, this.getCount(item) - count)
    }
    static create() {
        return {}
    }
}