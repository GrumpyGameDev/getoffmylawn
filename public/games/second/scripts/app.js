import { Display } from "./view/display.js"
import { GameController } from "./controller/gamecontroller.js"
import { View } from "./view/view.js"
document.addEventListener("DOMContentLoaded", () => {
    Display.initialize()
    View.draw()
})
document.addEventListener("keydown", event => {
    GameController.handleCommand("keydown", event.code)
})
document.addEventListener("keyup", event => {
    GameController.handleCommand("keyup", event.code)
})