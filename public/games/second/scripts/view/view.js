import { Display } from "./display.js"
export class View {
    static draw() {
        let ctx = Display.getContext()
        ctx.save()
        ctx.translate(Display.getWidth() / 2, Display.getHeight() / 2)
        ctx.scale(2, 2)
        ctx.ellipse(0, 0, 32, 32, 0, 0, 2 * Math.PI)
        ctx.fill()
        ctx.restore()
    }
}