let data = {

}
export class Display {
    static initialize() {
        data.canvas = document.getElementById("display")
        data.canvas.width = document.body.clientWidth
        data.canvas.height = document.body.clientHeight
        data.context = data.canvas.getContext("2d")
    }
    static getContext() {
        return data.context
    }
    static getWidth(){
        return data.canvas.width
    }
    static getHeight(){
        return data.canvas.height
    }
}